import React, { useState, useEffect } from "react";
import SwaggerUI from "swagger-ui-react";
import "swagger-ui-react/swagger-ui.css";

const MANIFEST_PATH = "/docs-manifest.json";
const TEXT = "text";
const JSON = "json";

const getFile = async (filePath, fileType = TEXT) => {
  const fileResponse = await fetch(filePath);

  if (fileType === TEXT) {
    return fileResponse.text();
  } else if (fileType === JSON) {
    return fileResponse.json();
  } else {
    return fileResponse;
  }
};

const openNav = () =>
  (document.getElementById("mySidenav").style.width = "250px");

const closeNav = () => (document.getElementById("mySidenav").style.width = "0");

const App = () => {
  const [manifest, setManifest] = useState();
  const [selectedFile, setSelectedFile] = useState();

  const selectFile = (file) => {
    setSelectedFile(file);
    closeNav();
  };

  useEffect(() => {
    const getManifest = async () => {
      const manifestFromServer = await getFile(MANIFEST_PATH, JSON);
      setManifest(manifestFromServer);
      setSelectedFile(manifestFromServer[0]);
    };

    getManifest();
  }, []);

  return (
    <div id="main">
      <button onClick={openNav}>
        <i className="fa fa-align-justify"></i>
      </button>
      <div id="mySidenav" className="sidenav">
        <h2 className="nav-title white">Documentation</h2>
        <button className="closebtn" onClick={closeNav}>
          <i className="fa fa-times"></i>
        </button>
        {manifest ? (
          <ul className="no-bullet white">
            {manifest.map((file, index) => (
              <li
                key={`${file.name.toLowerCase()}-${index}`}
                className="nav-item"
                onClick={() => selectFile(file)}
              >
                {file.name}
              </li>
            ))}
            <li
              className="nav-item last-nav-item"
              onClick={() => setSelectedFile()}
            >
              Clear
            </li>
          </ul>
        ) : (
          <div></div>
        )}
      </div>
      <div>
        {selectedFile ? <SwaggerUI url={selectedFile.path} /> : <div></div>}
      </div>
    </div>
  );
};

export default App;
